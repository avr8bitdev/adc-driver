/*
  MIT License

  Copyright (c) 2018 Mina Helmi

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the "Software"),
  to deal in the Software without restriction, including without limitation
  the rights to use, copy, modify, merge, publish, distribute, sublicense,
  and/or sell copies of the Software, and to permit persons to whom the
  Software is furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "ADC_functions.h"
#include "../Peripherals_addresses.h"
#include "../../basic_includes/bit_manip.h"
#include <avr/interrupt.h>


// --- internal --- //
#define ADC_CHANNEL_UNKNOWN 0xFF
#define ADC_CB_LENGTH_ConvEnded 10

static ADC_CB_ConvEnded_t ADC_CB_Queue_ConvEnded[ADC_CB_LENGTH_ConvEnded] = {0}; // callback functions pointers
static ADC_channel_t ADC_CB_CH[ADC_CB_LENGTH_ConvEnded] = {0}; // which channel for each callback
// ---------------- //

void ADC_vidInit(ADC_ref_t enumRefTypeCpy, ADC_adjust_t enumAdjustTypeCpy, ADC_prescaler_t enumPrescalarFactorCpy)
{
	ADC_vidSetRefVolt(enumRefTypeCpy);
	ADC_vidSetAdjustType(enumAdjustTypeCpy);
	ADC_vidSetPrescalerFactor(enumPrescalarFactorCpy);

	ADC_vidSetActiveChannel(ADC_single_ended_CH0);

	// reset callback functions
	for (u8 i = 0; i < ADC_CB_LENGTH_ConvEnded; i++)
		ADC_CB_Queue_ConvEnded[i] = 0;

	for (u8 i = 0; i < ADC_CB_LENGTH_ConvEnded; i++)
		ADC_CB_CH[i] = ADC_CHANNEL_UNKNOWN;

	ADC_vidEnable();
}

// ------ setters ------ //
inline void ADC_vidEnable(void)
{
	BIT_SET(ADC_CTRL_STATUS, 7);
}

inline void ADC_vidDisable(void)
{
	BIT_CLEAR(ADC_CTRL_STATUS, 7);
}

inline void ADC_vidEnableConvEndedINT(void)
{
	BIT_SET(ADC_CTRL_STATUS, 3);
}

inline void ADC_vidDisableConvEndedINT(void)
{
	BIT_CLEAR(ADC_CTRL_STATUS, 3);
}

inline void ADC_vidRegisterCB_ConvEnded(const ADC_CB_ConvEnded_t CBfuncCpy, ADC_channel_t enumChCpy, const u8 u8PosCpy)
{
	if (u8PosCpy < ADC_CB_LENGTH_ConvEnded)
	{
		ADC_CB_Queue_ConvEnded[u8PosCpy] = CBfuncCpy;
		ADC_CB_CH[u8PosCpy] = enumChCpy;
	}
}

inline void ADC_vidDeregisterCB_ConvEnded(const u8 u8PosCpy)
{
	if (u8PosCpy < ADC_CB_LENGTH_ConvEnded)
	{
		ADC_CB_Queue_ConvEnded[u8PosCpy] = 0;
		ADC_CB_CH[u8PosCpy] = ADC_CHANNEL_UNKNOWN;
	}
}

inline void ADC_vidSetRefVolt(ADC_ref_t enumRefTypeCpy)
{
	ADC_MUX &= 0b00111111;
	ADC_MUX |= enumRefTypeCpy;
}

inline void ADC_vidSetAdjustType(ADC_adjust_t enumAdjustTypeCpy)
{
	BIT_ASSIGN(ADC_MUX, 5, enumAdjustTypeCpy);
}

inline void ADC_vidSetPrescalerFactor(ADC_prescaler_t enumPrescalarFactorCpy)
{
	ADC_CTRL_STATUS &= 0b11111000;
	ADC_CTRL_STATUS |= enumPrescalarFactorCpy;
}

inline void ADC_vidSetActiveChannel(ADC_channel_t enumChCpy)
{
	ADC_MUX &= 0b11100000;
	ADC_MUX |= enumChCpy;
}

inline void ADC_vidStartConversion(void)
{
	BIT_SET(ADC_CTRL_STATUS, 6);
}
// ---------------------//


// ------ getters ------//
inline u8 ADC_u8isConvEnded(void)
{
	return !BIT_GET(ADC_CTRL_STATUS, 6);
}

inline u8 ADC_u8GetActiveChannel(void)
{
	return (ADC_MUX & 0b00011111);
}

u16 ADC_u16GetValue(ADC_channel_t enumChCpy)
{
	ADC_vidSetActiveChannel(enumChCpy);
	ADC_vidStartConversion();

	while (!ADC_u8isConvEnded()) // trap while conversion is not complete
	{}

	if (!BIT_GET(ADC_CTRL_STATUS, 3)) // if interrupt disabled
	{
	    BIT_SET(ADC_CTRL_STATUS, 4); // clear 'is_conversion_INT_ready' bit by writing 1 to it

	    /*
	    // trap until ISR is finished (while 'is_conversion_INT_ready' bit = 1)
		while (BIT_GET(ADC_CTRL_STATUS, 4))
		{}
		*/
	}


	return ADC_VALUE;
}
// ---------------------//


ISR(ADC_vect) // called when ADC conversion is complete and data is available
{
    BIT_SET(ADC_CTRL_STATUS, 4); // clear 'is_conversion_INT_ready' bit by writing 1 to it

	register u16 conv_val = ADC_VALUE; // catch ADC value immediately
	register u8 current_ch = ADC_u8GetActiveChannel(); // get current active channel

	for (u8 i = 0; i < ADC_CB_LENGTH_ConvEnded; i++)
	{
	     // if callback's assigned channel = current one, or it is a broadcast callback
		if ((ADC_CB_CH[i] == current_ch) || (ADC_CB_CH[i]== ADC_Channel_All))
		{
			if (ADC_CB_Queue_ConvEnded[i]) // if not empty (deregistered) callback
				ADC_CB_Queue_ConvEnded[i](conv_val);
		}
	}
}


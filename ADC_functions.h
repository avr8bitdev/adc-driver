/*
  MIT License

  Copyright (c) 2018 Mina Helmi

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the "Software"),
  to deal in the Software without restriction, including without limitation
  the rights to use, copy, modify, merge, publish, distribute, sublicense,
  and/or sell copies of the Software, and to permit persons to whom the
  Software is furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef MCAL_DRIVERS_ADC_DRIVER_ADC_FUNCTIONS_H_
#define MCAL_DRIVERS_ADC_DRIVER_ADC_FUNCTIONS_H_


#include "../../basic_includes/custom_types.h"

// this is used by callback register function to indicate a broadcast function
#define ADC_Channel_All (0xFF - 1)

// ADC reference voltage type
typedef enum
{
	ADC_ref_AREF_external  = 0b00000000,
	ADC_ref_VCC_internal   = 0b01000000, // with external capacitor at AREF pin
	ADC_ref_V2_56_internal = 0b11000000  // with external capacitor at AREF pin
} ADC_ref_t;

// ADC value adjust type
typedef enum
{
	ADC_adjust_high = 1, // 8-bits in ADCH + 2-bits in ADCL
	ADC_adjust_low  = 0, // 8-bits in ADCL + 2-bits in ADCH
} ADC_adjust_t;

// ADC channel selector
typedef enum
{
	ADC_single_ended_CH0 = 0b00000000,
	ADC_single_ended_CH1 = 0b00000001,
	ADC_single_ended_CH2 = 0b00000010,
	ADC_single_ended_CH3 = 0b00000011,
	ADC_single_ended_CH4 = 0b00000100,
	ADC_single_ended_CH5 = 0b00000101,
	ADC_single_ended_CH6 = 0b00000110,
	ADC_single_ended_CH7 = 0b00000111
} ADC_channel_t;

// ADC pre-scalar division factor
typedef enum
{// ADC operates between 50k Hz and 200k Hz
	ADC_prescaler_div_2   = 0b00000000,
	ADC_prescaler_div_4   = 0b00000010,
	ADC_prescaler_div_8   = 0b00000011,
	ADC_prescaler_div_16  = 0b00000100,
	ADC_prescaler_div_32  = 0b00000101,
	ADC_prescaler_div_64  = 0b00000110,
	ADC_prescaler_div_128 = 0b00000111,
} ADC_prescaler_t;

typedef void (*ADC_CB_ConvEnded_t)(u16);  // conversion complete callbacks

void ADC_vidInit(ADC_ref_t enumRefTypeCpy, ADC_adjust_t enumAdjustTypeCpy, ADC_prescaler_t enumPrescalarFactorCpy);

// ------ setters ------ //
void ADC_vidEnable(void);
void ADC_vidDisable(void);

void ADC_vidEnableConvEndedINT(void);
void ADC_vidDisableConvEndedINT(void);

void ADC_vidRegisterCB_ConvEnded(const ADC_CB_ConvEnded_t CBfuncCpy, ADC_channel_t enumChCpy, const u8 u8PosCpy);
void ADC_vidDeregisterCB_ConvEnded(const u8 u8PosCpy);

void ADC_vidSetRefVolt(ADC_ref_t enumRefTypeCpy);

void ADC_vidSetAdjustType(ADC_adjust_t enumAdjustTypeCpy);

void ADC_vidSetPrescalerFactor(ADC_prescaler_t enumPrescalarFactorCpy);

void ADC_vidSetActiveChannel(ADC_channel_t enumChCpy);

void ADC_vidStartConversion(void);
// ---------------------//

// ------ getters ------//
u8 ADC_u8isConvEnded(void);

u8 ADC_u8GetActiveChannel(void);

u16 ADC_u16GetValue(ADC_channel_t enumChCpy);
// ---------------------//


#endif /* MCAL_DRIVERS_ADC_DRIVER_ADC_FUNCTIONS_H_ */

